/*
 * Non_Conductive_Mode_Function.cpp
 * Created: 3-7-2018 12:54:59
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Non_Conductive_Mode_Function (void)
{
	//Stop Down movement when safety sensor change state.
	if (ConductiveM_HMI == true || Setup_Stroke_TPS == true)
	{	
		if (digitalRead(FOOT_DOWN1) == HIGH)
		{
			if (Safety_Sensor_Switched_State == true && !Non_Conductive_Stop1 == true)
			{
				Non_Conductive_Stop1 = true;
			}
		}
	}
	//Activate variable to Non_Conductive_Stop2 to de-active Non_Conductive_Stop1 when deactivating Down Pedal and then activating Down Pedal for the second time to press.
	if (Non_Conductive_Stop1 && digitalRead (FOOT_DOWN1) == false)
	{
		Non_Conductive_Stop2 = true;
	}
	if (Non_Conductive_Stop2 == true && digitalRead (FOOT_DOWN1) == true)
	{
		Non_Conductive_Stop1 = false;
	}
	//Use Up Pedal to reset both conditions (Non_conductive_Stop1 & Non_Conductive_Stop2) to ensure the procedure will be reset.
	if (digitalRead(FOOT_UP) == true || Up_Signal_MCU2 == true)
	{
		Non_Conductive_Stop1 = false;
		Non_Conductive_Stop2 = false;
	}
}