#include "Main_Siemens_Automation_824MSPe.h"
#include "Arduino.h"

//****Wire Communication
#include <Wire.h>
//Send Bytes to MCU1.
byte Send_to_MCU1_Standard_Machine_Control_0 = 0;
byte Send_to_MCU1_Standard_Machine_Control_1 = 0;
//Receive Bytes from MCU1.
byte Receive_from_MCU1_Standard_Machine_Control_0 = 0;
byte Receive_from_MCU1_Standard_Machine_Control_1 = 0;
//Wire Case.
byte Wirecase = 0;

//****Modbus Communication
#include <ModbusRtu.h>
#define ID   1
ModbusSerial<decltype(Serial)> mySerial(&Serial);
//This is device ID and RS-232 or USB-FTDI
Modbus device(ID, 0);
boolean led;
int8_t state = 0;
unsigned long tempus;
//Data array for modbus network sharing
uint16_t au16data[123];

//Analog Read variables.
#include "ResponsiveAnalogread.h"
ResponsiveAnalogRead CET(AI_CET, true);
ResponsiveAnalogRead VAC(AI_VAC_SWITCH, true);
ResponsiveAnalogRead PRESSUREWITCH(AI_PRESSURE_SWITCH, true);

//Global Variables.
bool Safety_Sensor_Error;
bool Machine_Init_State;
bool Machine_Idle_State;
bool Machine_Run_State;
bool Toolingcontact_Switched_State;
bool Safety_Sensor_Switched_State;
bool Non_Conductive_Stop1;
bool Non_Conductive_Stop2;
bool Press_Complete;
bool TPS_Error;
bool Dwell_Time_Active;
bool Fastener_Detection_Error;
bool Fastener_Length_Error;
bool MAS_Feed_Fastener_Active;
bool MAS_Blow_Off_Active;
bool MAS_Vibration_Active;
bool Hyd_Cyl_Down_Release_Active;
bool Hyd_Cyl_Up_Release_Active;
bool Press_Complete_Active;
int TOS;
bool Vac_Without_Fastener_Down_Hold;
bool Vac_With_Fastener_Down_Hold;
bool Hyd_Cyl_Dow_Release_Active_1;
bool Vacuum_Detection_Measurement_Active;
int Fastener_Detection_Zone;
bool Up_Travel_Cylinder_Stop;
bool FLD_Measurment_Active;
bool FD_Retry_Set;
bool SSSF_Safety_Sensor_Error;
bool Error_Message;
bool Machine_State_Robot_Mode_Active;
bool EMG_Error_Reset_Local;
bool Robot_Error_Reset_Input;
bool Robot_Press_Signal_Active;
bool All_At_Position;
bool Lower_Tool_Pin_Up_Sensor;
bool Lower_Tool_Pin_Down_Sensor;
int Analog_CET;
int Analog_VAC;
int Analog_PRESSURESWITCH;
bool FL_Setup_Stroke_Reset;
bool Robot_Shuttle_Lock;
int MCU2_Version_Number;
bool Shuttle_Extend_Check_Global;
bool Setup_Stroke_TPS;

//Global Variables from or Set to MCU2.
//Wire communication.
//Receive from MCU1.
bool Robot_Mode_Active_MCU2;
//Receive from MCU1.
bool Robot_Error_Reset_MCU2;
//Receive from MCU1.
bool EMG_Error_Reset_MCU2;
//Send to MCU1.
bool EMG_System_Active_MCU2;
//Send to MCU1.
bool All_At_Position_MCU2;
//Receive from MCU1
bool Robot_Press_Active_MCU2;
//Receive from MCU1.
bool Lower_Tool_Pin_Down_Sensor_MCU2;
//Receive from MCU1.
bool Lower_Tool_Pin_Up_Sensor_MCU2;
//Lower Tool Pin Up movement Output.
bool MCU_Lower_Tool_Pin_Up_Output;
//Lower Tool Pin Down movement Output.
bool MCU_Lower_Tool_Pin_Down_Output;
//Robot Shuttle Lock
bool Robot_Shuttle_Lock_MCU2;
//Dwell Time send to MCU1.
bool Dwell_Time_Active_MCU2;
//Setup Stroke send to MCU2.
bool Setup_Stroke_MCU2;
//Shuttle Retract Signal from MCU2.
bool Shuttle_Retract_MCU2;
//Shuttle Extend Signal from MCU2.
bool Shuttle_Extend_MCU2;
//Feed Fastener Signal from MCU1.
bool Robot_Feed_Fastener_MCU2;
//Hydraulic Up Signal to MCU1.
bool Up_Signal_MCU2;
//Down Solenoid Block signal send to MCU2. Output Signal to MCU_1 to block Down movement during a MAS_Eject.
bool Down_Solenoid_Block_MCU2;

//Quality (QA) Variables.
int Vacuum_Switch_AI;

//Modbus HMI Variables.				
bool Run_Setup_Mode_HMI;				
bool ConductiveM_HMI;
bool Non_ConductiveM_HMI;			
int Dwell_Time_HMI;						
//Upper TPS Value.
int TPS_Calibrated_Upper_Value_HMI;										
//Lower TPS Value.
int TPS_Calibrated_Lower_Value_HMI;
//TPS Offset value.
int TPS_Offset_Value_HMI;							
//Setup Stroke HMI.
bool Setup_Stroke_HMI;
//Fastener Detection HMI.
bool Fastener_Detection_HMI;
//Fastener Detection Sensitivity.
int Fastener_Detection_Sensitivity_HMI;
//Fastener Length HMI.
bool Fastener_Length_HMI;
//Fastener Detection Error HMI.
bool Fastener_Detection_Error_HMI;
//Fastener Length Error HMI.
bool Fastener_Length_Error_HMI;
//Fastener Detection Calibrated Value HMI.
int Fastener_Detection_Calibrated_Value_HMI;
//Fastener Length Calibration Value HMI.
int Fastener_Length_Calibrated_Value_HMI;
//Fastener Detection Shuttle Calibration Position HMI (Shuttle position). This position will be set manual in Advanced Options.
int Fastener_Detection_Shuttle_Calibration_Pos_HMI;							
//Fastener Detection Shuttle Offset_Position HMI.
int Fastener_Detection_Shuttle_Offset_Pos_HMI;
//Fastener Length Offset HMI.
int Fastener_Length_Sensitifity_HMI;
//Shuttle Extend Error.
bool Shuttle_Extend_Error;
//Shuttle Retract Error.
bool Shuttle_Retract_Error;
//HMI Reset.
bool Error_Set_HMI;
//EMG Error HMI.
bool EMG_Error_HMI;
//EMG Active.
bool EMG_System_Active_HMI;
//Vacuum with Fastener.
int Fastener_Detection_Baseline_HMI;
//Top Of Stroke Position.
int TOS_Position_HMI;
//Modbus Coil 34 Read.
int Read_Coil_34;
//Modbus Coil 34 Write.
int Write_Coil_34;
//Modbus Coil 38 Read.
int Read_Coil_38;
//Modbus Coil 38 Write.
int Write_Coil_38;
//Modbus Coil 40 Read.
int Read_Coil_40;
//Modbus Coil 40 Write.
int Write_Coil_40;
//Modbus Coil 48 Read.
int Read_Coil_48;
//Modbus Coil 48 Write.
int Write_Coil_48;
//Modbus Coil 49 Read.
bool Read_Coil_49;
//Modbus Coil 49 Write.
bool Write_Coil_49;
//Modbus Coil 73 Read.
int Read_Coil_73;
//Modbus Coil 73 Write.
int Write_Coil_73;
//Up Travel HMI.
int Up_Travel_HMI;
//TPS Set HMI.
bool TPS_Set_HMI;
//Error Number HMI
int Error_Number_HMI;
//Fastener Detection Retry HMI
int Fastener_Detection_Retry_HMI;
//Error screen active.
int Error_Screen_Active_HMI;

//MAS HMI Variables.
//Auto Tooling HMI.
int Tooling_Mode_HMI;
//MAS Activation.
bool MAS_Vibrate_Mode_HMI;
//MAS Manual feed signal.
bool MAS_Feed_HMI;
//Feed Fastener time.
int MAS_Blow_Off_Time_HMI;												
//MAS Eject Time. Is time that Push bar will be opened before blow off is active.
int MAS_Eject_Time_HMI;															
//MAS Vibration Time when a fastener is send.								
int MAS_Vibration_Time_HMI;												
//MAS Vibration percentage. Use MAS_Automatic to turn on vibration continuesly or intermittent.
int MAS_Vibration_Percentage_HMI;							
//MAS Delay time. Delay before shuttle will move to front position.
int MAS_Delay_Time_HMI;
//Vacuum On HMI.
int Vacuum_Mode_HMI;

//Global Timer Variables.
bool Timer1_Q;
bool Timer1_Start;

//Wire Delay Timer.
int WireMs = 10;
unsigned long WireStart;

//Main System Setup Code (Initialization)
void setup()
{	
	//Wire Communication Start. Send Byte to device #4.
	Wire.begin(4);
	Wire.onRequest(RequestEvent_Send);
	Wire.onReceive(RequestEvent_Receive);
	
	//Modbus Communication Setup.
	//start communication.
	//device.begin(&mySerial, 480000);
	//device.begin(&mySerial, 128000);
	device.begin(&mySerial, 9600);
	
	//Analog Setup
	CET.setAnalogResolution(4096);
	VAC.setAnalogResolution(4096);
	PRESSUREWITCH.setAnalogResolution(4096);
	
	//Machine E-Stop Input #2. E-Stop 2 needed to ensure 24Volt is continuously on.
	pinMode(ESTOP2, INPUT_PULLDOWN);
	//Initialize Down Solenoid Pin.
	pinMode(DOWN_SOLENOID, OUTPUT);
	//Initialize Down1 Pedal.
	pinMode(FOOT_DOWN1, INPUT_PULLDOWN);
	//Initialize Down2 Pedal.
	pinMode(FOOT_DOWN2, INPUT_PULLDOWN);
	//Initialize Up Pedal.
	pinMode(FOOT_UP, INPUT_PULLDOWN);
	//Initialize Safety Sensors.
	pinMode(SAFETY_SWITCH_NC, INPUT_PULLDOWN);
	pinMode(SAFETY_SWITCH_NO, INPUT_PULLDOWN);
	//Set analog read resolution to 12bits (0-4095) instead of standard 10bits (0-1023).
	analogReadResolution(12);
	//Set analog write resolution to 12bits (0-4095) instead of standard 10bits (0-1023).
	analogWriteResolution(12);
	//MAS1 Eject.
	pinMode(MAS1_EJECT, OUTPUT);
	//MAS1 BlowOff.
	pinMode(MAS1_BLOWOFF, OUTPUT);
	//MAS1 Power On.
	pinMode(MAS1_DIGITAL, OUTPUT);
	//Shuttle Extend Sensor.
	pinMode(SHUTTLE_EXTENDED, INPUT_PULLDOWN);
	//Shuttle Retract Sensor.
	pinMode(SHUTTLE_RETRACTED, INPUT_PULLDOWN);
	//Shuttle Valve.
	pinMode(SHUTTLE_EXTEND, OUTPUT);
	
	//Setup MAS Function.
	void Setup_MAS_Function();
	//Setup Feed Fastener Function.
	void Setup_Feed_Fastener_Function();
	//Setup Press Complete Function.
	void Setup_Press_Complete_Function();
	//Setup MAS Blow Off Function.
	void Setup_MAS_Blow_Off_Function();
	//Setup Shuttle Function.
	void Setup_Shuttle_Function();
	//Setup Dwell Time Function.
	void Setup_Dwell_Time_Function();
	//Setup Modbus Communication Function.
	void Setup_Modbus_Communication_Function();
	//Setup Hydraulic Cylinder Function.
	void Setup_Hydraulic_Cylinder_Function();
	//Setup Fastener Detection Function.
	void Setup_Fastener_Detection_Function();
	//Setup Fastener Length Detection Function.
	void Setup_Fastener_Length_Detection_Function();
	//Setup Conductive Mode Function.
	void Setup_Conductive_Mode_Function_Function();
	//Setup TPS Function.
	void Setup_Tooling_Protection_System_Function();
}

//Wire Communication. Send Bytes to MCU1.
void Wire_Communication_Send()
{	
	//Send Byte #0. "Send_to_MCU1_Standard_Machine_Control_0".
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,0,Error_Set_HMI);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,1,EMG_System_Active_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,2,All_At_Position_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,3,Press_Complete);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,4,Dwell_Time_Active_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,5,Setup_Stroke_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,6,Shuttle_Extend_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_0,7,Shuttle_Retract_MCU2);
	//Send Byte #1. "Send_to_MCU2_Standard_Machine_Control_1".
	bitWrite(Send_to_MCU1_Standard_Machine_Control_1,0,Up_Signal_MCU2);
	bitWrite(Send_to_MCU1_Standard_Machine_Control_1,1,Down_Solenoid_Block_MCU2);
	
	Wire.write(Send_to_MCU1_Standard_Machine_Control_0);
	Wire.write(Send_to_MCU1_Standard_Machine_Control_1);
}

//Wire Communication. Send Bytes to MCU1.
void RequestEvent_Send()
{
	Wire.write(Send_to_MCU1_Standard_Machine_Control_0);
	Wire.write(Send_to_MCU1_Standard_Machine_Control_1);
}

//Wire Communication. Receive Bytes from MCU1.
void RequestEvent_Receive(int)
{
	//While Loop.
	while(Wire.available())
	{
		Receive_from_MCU1_Standard_Machine_Control_0 = Wire.read();
		Receive_from_MCU1_Standard_Machine_Control_1 = Wire.read();
	}
	//Read bit 0 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	Robot_Mode_Active_MCU2			= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 0);
	//Read bit 1 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	Robot_Error_Reset_MCU2			= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 1);
	//Read bit 2 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	EMG_Error_Reset_MCU2			= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 2);
	//Read bit 3 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	Robot_Press_Active_MCU2			= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 3);
	//Read bit 4 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	MCU_Lower_Tool_Pin_Up_Output	= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 4);
	//Read bit 5 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	MCU_Lower_Tool_Pin_Down_Output	= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 5);
	//Read bit 6 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	Lower_Tool_Pin_Up_Sensor_MCU2	= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 6);
	//Read bit 7 from Byte 0.Receive_from_MCU1_Standard_Machine_Control_0.
	Lower_Tool_Pin_Down_Sensor_MCU2	= bitRead(Receive_from_MCU1_Standard_Machine_Control_0, 7);
	
	//Read bit 0 from Byte 1.Receive_from_MCU1_Standard_Machine_Control_1.
	Robot_Feed_Fastener_MCU2		= bitRead(Receive_from_MCU1_Standard_Machine_Control_1, 0);
	//Read bit 1 from Byte 1.Receive_from_MCU1_Standard_Machine_Control_1.
	Robot_Shuttle_Lock_MCU2			= bitRead(Receive_from_MCU1_Standard_Machine_Control_1, 1);
}

//Main program code into the continuous loop() function.
void loop()
{	
	//Modbus Communication Loop.
	//Poll messages
	//Blink led pin on each valid message
	state = device.poll( au16data, 123 );
	if (state > 4)
	{tempus = millis() + 50; digitalWrite(26, HIGH);}
	if (millis() > tempus) digitalWrite(26, LOW );
	Io_Poll();
	
	//Analog read CET.
	CET.update();
	Analog_CET = CET.getValue();
	//Analog read Vacuum.
	VAC.update();
	Analog_VAC = VAC.getValue();
	//Analog read Pressure Switch.
	PRESSUREWITCH.update();
	Analog_PRESSURESWITCH = PRESSUREWITCH.getValue();
	
	//Function blocks
	Feed_Fastener_Function();
	Footpedal_Function();
	Initialization_Function();
	Machine_State_Function();
	MAS_Function();
	Safety_Sensor_State_Function();
	Timer_Function();
	Toolingcontact_Function();
	Conductive_Mode_Function();
	Non_Conductive_Mode_Function();
	Alarm_Function();
	Tooling_Protection_System_Function();
	Setup_Stroke_Vacuum_Detection_Function();
	Fastener_Detection_Function();
	Fastener_Length_Function();
	Press_Complete_Function();
	MAS_Blow_Off_Function();
	Shuttle_Function();
	Hydraulic_Cylinder_Function();
	Dwell_Time_Function();
	Vacuum_Generator_Function();
	Safety_Block_Function();
	TOS_Function();
	Setup_Stroke_TPS_Function();
	Setup_Stroke_Fastener_Length_Function();
	Modbus_Communication_Function();
	Up_Travel_Function();
	Robot_Communication_Function();
	All_At_Position_Function();
	MCU_Version_Function();
	
	//Wire Communication.
	Wire_Communication_Send();
	//Wire Delay Case.
	if ((millis() - WireMs) >= WireStart)
	{
		WireStart = millis();
		switch(Wirecase)
		{
			//Case 0.
			case 0:
			//Dummy.
			Wirecase = 1;
			break;
			//Case 1.
			case 1:
			//Dummy.
			Wirecase = 0;
			break;			
		}
	}
}

//Modbus communication between Master and Slave (PCB board = Slave).
void Io_Poll()
{
	//Read digital outputs/inputs from HMI -> au16data[2].
	Run_Setup_Mode_HMI										=	bitRead( au16data[2],0);				//32
	ConductiveM_HMI											=	bitRead( au16data[2],1);				//33
	
	if (Read_Coil_34 == true)
	{Setup_Stroke_HMI										=	bitRead( au16data[2],2);}				//34	
	Fastener_Detection_HMI									=	bitRead( au16data[2],3);				//35**1
	Fastener_Length_HMI										=	bitRead( au16data[2],4);				//36**1
	MAS_Vibrate_Mode_HMI									=	bitRead( au16data[2],5);				//37
	if (Read_Coil_38 == true)	
	{MAS_Feed_HMI											= 	bitRead( au16data[2],6);}				//38
	if (Read_Coil_40 == true)
	{TPS_Set_HMI											=	bitRead( au16data[2],8);}				//40
	if (Read_Coil_49 == true)
	{Error_Screen_Active_HMI								=	bitRead( au16data[2],9);}				//41
	
	//Write digital outputs/inputs to HMI -> au16data[2] & [3].
	if (Write_Coil_38 == true)
	{bitWrite (au16data[2],6,MAS_Feed_HMI);}															//38
	if (Write_Coil_34 == true)
	{bitWrite (au16data[2],2,Setup_Stroke_HMI);}														//34	
	bitWrite (au16data[2],7,Press_Complete);															//39
	if (Write_Coil_40 == true)
	{bitWrite (au16data[2],8,TPS_Set_HMI);}																//40
	if (Write_Coil_49 == true)
	{bitWrite (au16data[2],9,Error_Screen_Active_HMI);}													//41
	bitWrite (au16data[3],1,EMG_System_Active_HMI = 1);													//49		
	
	//Read digital Error outputs/inputs from HMI -> au16data[3].
	if(Read_Coil_48 == true)
	{Error_Set_HMI											=	bitRead( au16data[3],0);}				//48
				
	//Write digital Error outputs/inputs -> au16data[1].
	if(Write_Coil_48 == true)
	{bitWrite (au16data[3],0, Error_Set_HMI);}															//48

	//Read register values from HMI -> au16data[10].
	TPS_Offset_Value_HMI									=	(au16data[50]);							//Register:50**200
	Dwell_Time_HMI											=	(au16data[51]);							//Register:51**0
	Fastener_Detection_Sensitivity_HMI						= 	(au16data[52]);							//Register:52**100
	Fastener_Length_Sensitifity_HMI							=	(au16data[53]);							//Register:53
	Fastener_Detection_Shuttle_Calibration_Pos_HMI			= 	(au16data[54]);							//Register:54**3500
	Fastener_Detection_Shuttle_Offset_Pos_HMI				= 	(au16data[55]);							//Register:55**400
	MAS_Blow_Off_Time_HMI									=	(au16data[56]);							//Register:56**3000
	MAS_Eject_Time_HMI										=	(au16data[57] = 200);					//Register:57**200
	MAS_Vibration_Time_HMI									=	(au16data[58]);							//Register:58**2000
	MAS_Vibration_Percentage_HMI							=	(au16data[59]);							//Register:59**3400
	MAS_Delay_Time_HMI										=	(au16data[60]);							//Register:60
	TOS_Position_HMI										= 	(au16data[61]);							//Register:61**3600
	Tooling_Mode_HMI										=	(au16data[62]);							//Register:62**0
	Vacuum_Mode_HMI											=	(au16data[63]);							//Register:63**0
	Up_Travel_HMI											=	(au16data[64]);							//Register:64**0
	Fastener_Detection_Retry_HMI							=	(au16data[65]);							//Register:65**0
	
	//Read register values from HMI -> au16data[70].
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{TPS_Calibrated_Upper_Value_HMI							=	(au16data[70]);}						//Register:70
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{TPS_Calibrated_Lower_Value_HMI							=	(au16data[71]);}						//Register:71
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)	
	{Fastener_Length_Calibrated_Value_HMI					=	(au16data[72]);}						//Register:72
	if(Write_Coil_73 == false)
	{Error_Number_HMI										=	(au16data[73]);}						//Register:73
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{Fastener_Detection_Baseline_HMI						=	(au16data[74]);}						//Register:74
	if (!Setup_Stroke_HMI == true && !TPS_Set_HMI == true)
	{Fastener_Detection_Zone								=	(au16data[75]);}						//Register:75
	
	//Write register values to HMI -> au16data[70].
	(au16data[70])											=	TPS_Calibrated_Upper_Value_HMI;			//Register:70
	(au16data[71])											=	TPS_Calibrated_Lower_Value_HMI;			//Register:71
	(au16data[72])											=	Fastener_Length_Calibrated_Value_HMI;	//Register:72
	if(Write_Coil_73 == true)
	{(au16data[73])											=	Error_Number_HMI;}						//Register:73
	(au16data[74])											=	Fastener_Detection_Baseline_HMI;		//Register:74**1250
	(au16data[75])											=	Fastener_Detection_Zone;				//Register:75
	
	//Send MCU1 version number to HMI [132]
	(au16data[76])											=	MCU2_Version_Number;					//Register:76
	
	//Write values direct from coil to HMI -> au16data[80].
	(au16data[80])											=	analogRead(AI_PRESSURE_SWITCH);			//Register:80
	(au16data[81])											=	analogRead(AI_CET);						//Register:81
	(au16data[82])											=	analogRead(AI_VAC_SWITCH);				//Register:82

	//Diagnose communication -> au16data[90].
	au16data[90] = device.getInCnt();
	au16data[91] = device.getOutCnt();
	au16data[92] = device.getErrCnt();
}