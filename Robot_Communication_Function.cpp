/*
 * Robot_Communication_Function.cpp
 * Created: 9-9-2018 17:25:20
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.

//Function Code.
void Robot_Communication_Function(void)
{
	//Robot Shuttle Lock.
	if (Robot_Shuttle_Lock_MCU2 == true)
	{
		Robot_Shuttle_Lock = true;
	}
	else
	{
		Robot_Shuttle_Lock = false;
	}
	//Set All At Position Signal. Send Value to MCU1.
	if (All_At_Position == true)
	{
		All_At_Position_MCU2 = true;
	} 
	else
	{
		All_At_Position_MCU2 = false;
	}
	//Error reset Input from MCU1. Reset Signal from Robot.
	if (Robot_Error_Reset_MCU2 == true)
	{
		Robot_Error_Reset_Input = true;
	} 
	else
	{
		Robot_Error_Reset_Input = false;
	}
	//Key Switch to set machine in Robot Mode. Input from MCU1.
	if (Robot_Mode_Active_MCU2 == true)
	{
		Machine_State_Robot_Mode_Active = true;
	} 
	else
	{
		Machine_State_Robot_Mode_Active = false;
	}
	//EMG Error Reset. Signal from MCU1. Reset Signal from Robot.
	if (EMG_Error_Reset_MCU2 == true)
	{
		EMG_Error_Reset_Local = true;
	} 
	else
	{
		EMG_Error_Reset_Local = false;
	}
	//Send EMG Status to MCU1. MCU1 send EMG Status to Robot.
	if (EMG_System_Active_HMI == true)
	{
		EMG_System_Active_MCU2 = true;
	} 
	else
	{
		EMG_System_Active_MCU2 = false;
	}
	//Robot Press Signal.
	if (Robot_Press_Active_MCU2 == true)
	{
		Robot_Press_Signal_Active = true;
	} 
	else
	{
		Robot_Press_Signal_Active = false;
	}
	//Lower Tool Pin Up Position.
	if (Lower_Tool_Pin_Up_Sensor_MCU2 == true)
	{
		Lower_Tool_Pin_Up_Sensor = true;
	} 
	else
	{
		Lower_Tool_Pin_Up_Sensor = false;
	}
	//Lower Tool Pin Down Position.
	if (Lower_Tool_Pin_Down_Sensor_MCU2 == true)
	{
		Lower_Tool_Pin_Down_Sensor = true;
	} 
	else
	{
		Lower_Tool_Pin_Down_Sensor = false;
	}
	//Dwell Time Robot.
	if (Dwell_Time_Active == true)
	{
		Dwell_Time_Active_MCU2 = true;
	}
	else
	{
		Dwell_Time_Active_MCU2 = false;
	}
	//Send Shuttle Retract position to MCU1.
	if (digitalRead(SHUTTLE_RETRACTED) == HIGH)
	{
		Shuttle_Retract_MCU2 = true;
	} 
	else
	{
		Shuttle_Retract_MCU2 = false;
	}
	//Send Shuttle Retract position to MCU1.
	if (digitalRead(SHUTTLE_EXTENDED) == HIGH)
	{
		Shuttle_Extend_MCU2 = true;
	}
	else
	{
		Shuttle_Extend_MCU2 = false;
	}
}
