/*
 * Hydraulic_Cylinder_Function.cpp
 * Created: 17-7-2018 15:51:53
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Shuttle_Selection_Up_Travel;
bool Tool_Mode_Lock;
bool TPS_Cylinder_Lock;

//Timer Variables.
unsigned long Timer_9;
//Set timer to true when timer is finished.
bool TimedOut_9 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_9;
//Reset Time.
int TPS_Lock_Reset_Time = 1000;
//TPS Lock Reset
bool TPS_Lock_Reset;

//CET set value.
int CetValue_Hydraulic_Cylinder_Function;

//Setup Code.
void Setup_Hydraulic_Cylinder_Function(void)
{
	//Timer initialization.
	TimedOut_9 = false;
	//Start Timer.
	Timer_9 = millis();
}

//Function Code.
void Hydraulic_Cylinder_Function(void)
{	
	//Read CET Analog input.
	CetValue_Hydraulic_Cylinder_Function = Analog_CET;
	
	//Stop Hydraulic Cylinder Down Movement after Error. Not possible to move cylinder down when TPS is not Set.
	if (Error_Screen_Active_HMI == true || TPS_Cylinder_Lock == true || !EMG_System_Active_HMI == true || (Safety_Sensor_Error == true || SSSF_Safety_Sensor_Error == true) || TPS_Error == true || Fastener_Detection_Error == true || Fastener_Length_Error == true || Shuttle_Extend_Error == true || Shuttle_Retract_Error == true)
	{
		Hyd_Cyl_Down_Release_Active = false;
	}
	else
	{
		Hyd_Cyl_Down_Release_Active = true;
	}
	//Hydraulic Cylinder Up Movement until TOS (Top of Stroke).
	if (Hyd_Cyl_Down_Release_Active == false && (CetValue_Hydraulic_Cylinder_Function < TOS) || (Press_Complete_Active == true && !Dwell_Time_Active == true) || Shuttle_Selection_Up_Travel == true)
	{
		Hyd_Cyl_Up_Release_Active = true;
	}
	else
	{
		Hyd_Cyl_Up_Release_Active = false;
	}
	//Hydraulic Cylinder Up Movement until TOS (Top of Stroke) after Press Complete or after Up Travel Percentage.
	if (Press_Complete == true)
	{
		Press_Complete_Active = true;
	} 
	else if ((TOS < CetValue_Hydraulic_Cylinder_Function) || Up_Travel_Cylinder_Stop == true)
	{
		Press_Complete_Active = false;
	}
	//No Down Movement possible during Vacuum measurement.
	if (Up_Signal_MCU2 == true || Vac_Without_Fastener_Down_Hold == true || Vac_With_Fastener_Down_Hold == true || Shuttle_Extend_Check_Global == false)
	{
		Down_Solenoid_Block_MCU2 = true;
		Hyd_Cyl_Dow_Release_Active_1 = true;
	} 
	else if (digitalRead(MAS1_EJECT) == LOW && !Vac_Without_Fastener_Down_Hold == true && !Vac_With_Fastener_Down_Hold == true && Shuttle_Extend_Check_Global == true)
	{
		Down_Solenoid_Block_MCU2 = false;
		Hyd_Cyl_Dow_Release_Active_1 = false;
	}
	//Reset Errors above TOS.
	if (CetValue_Hydraulic_Cylinder_Function >= TOS)
	{
		// General Error reset.
		Error_Message = false;
		//TPS error will be reset at TOS.
		TPS_Error = false;
		//FL error will be reset at TOS.
		Fastener_Length_Error = false;
		//SS error will be reset at TOS.
		Safety_Sensor_Error = false;
		//Faster Detection reset at TOS.
		Fastener_Detection_Error = false;
	}
	//Move Cylinder to TOS when changing from Manual, ABFT to Shuttle Mode.
	if (Tooling_Mode_HMI == 2 && !Tool_Mode_Lock == true)
	{
		Shuttle_Selection_Up_Travel = true;
		Tool_Mode_Lock = true;
	}
	else if (CetValue_Hydraulic_Cylinder_Function >= TOS)
	{
		Shuttle_Selection_Up_Travel = false;
	}
	if (Tooling_Mode_HMI == 0 || Tooling_Mode_HMI == 1)
	{
		Tool_Mode_Lock = false;
	}
	//TPS Setup Cylinder Lock Timer.
	if ((TimedOut_9 == true) && ((millis() - Timer_9) > INTERVAL_9))
	{
		//Timed out to deactivate timer.
		TimedOut_9 = true;
		
		//Check if Swap Bit is activated.
		if (TPS_Lock_Reset == true)
		{
			//De-active Timer.
			TPS_Lock_Reset = false;
			TimedOut_9 = false;
		}
	}
	//Set Swap Bit Time.
	INTERVAL_9 = TPS_Lock_Reset_Time;
		
	//Timer Start.
	if (Setup_Stroke_HMI == true && TimedOut_9 == false)
	{
		TimedOut_9 = true;
		Timer_9 = millis();
		TPS_Lock_Reset = true;
	}
	//TPS Setup Cylinder Lock. TPS lock will ensure that it is not possible to move the Ram down when TPS is not set.
	if (Setup_Stroke_HMI == false && !TPS_Lock_Reset == true && (TPS_Calibrated_Upper_Value_HMI == 0 || TPS_Calibrated_Lower_Value_HMI == 0))
	{
		if (TPS_Calibrated_Upper_Value_HMI == 0 || TPS_Calibrated_Lower_Value_HMI == 0)
		{
			TPS_Cylinder_Lock = true;
		}
	}
	else if (Setup_Stroke_HMI == true)
	{
		TPS_Cylinder_Lock = false;
	}
	else if (Setup_Stroke_HMI == false && (TPS_Calibrated_Upper_Value_HMI > 0 || TPS_Calibrated_Lower_Value_HMI > 0))
	{
		TPS_Cylinder_Lock = false;
	}
}

