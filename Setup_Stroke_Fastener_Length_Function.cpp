/*
 * Setup_Stroke_Function.cpp
 * Created: 4-7-2018 10:50:55
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Stop_Read_FLD_Value;
int Init_FLD_Values;

//Function Code.
void Setup_Stroke_Fastener_Length_Function(void)
{		
	if (Fastener_Length_HMI == true && (Setup_Stroke_HMI == true && !TPS_Set_HMI == true) && !FL_Setup_Stroke_Reset == true)
	{
		if (Init_FLD_Values == true)
		{
			Fastener_Length_Calibrated_Value_HMI = 0;
			Init_FLD_Values = false;
		}
		if (Toolingcontact_Switched_State == true && !Stop_Read_FLD_Value == true && !FL_Setup_Stroke_Reset == true)
		{
			FL_Setup_Stroke_Reset = true;
			Stop_Read_FLD_Value = true;
			Fastener_Length_Calibrated_Value_HMI = Analog_CET;
		}
	}
	//Reset Fastener Length Measurement Active.
	else if (FL_Setup_Stroke_Reset == false)
	{
		Init_FLD_Values = true;
		Stop_Read_FLD_Value = false;
	}
}
