/*
 * MAS_Function.cpp
 * Created: 25-6-2018 14:54:52
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
int MAS1_Amplifier_Value;

//Timer Variables.
unsigned long Timer_2;
//Set timer to true when timer is finished.
bool TimedOut_2 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_2;

//Setup Code.
void Setup_MAS_Function(void)
{
	//Timer initialization.
	TimedOut_2 = false;
	//Start Timer.
	Timer_2 = millis();
}

//Function Code.
void MAS_Function(void)
{	
	if ((!TimedOut_2) && ((millis() - Timer_2) > INTERVAL_2))
	{
		//Timed out to deactivate timer.
		TimedOut_2 = true;

		//Check if MAS Blow Off is activated.
		if (MAS_Vibration_Active == true)
		{
			//De-active MAS Blow Off Timer.
			MAS_Vibration_Active = false;
		}
	}
	//Set MAS Blow Off Time.
	INTERVAL_2 = MAS_Vibration_Time_HMI;
	
	//Timer Start. Activate MAS Blow Off.
	if (MAS_Blow_Off_Active == true && (INTERVAL_2 > 0))
	{
		TimedOut_2 = false;
		Timer_2 = millis();
		MAS_Vibration_Active = true;
	}
	
	//MAS Vibration starts after MAS Blow Off.
	if ((!MAS_Blow_Off_Active == true && MAS_Vibration_Active == true) || MAS_Vibrate_Mode_HMI == true)
	{
		digitalWrite(MAS1_DIGITAL, HIGH);
		//10Volt = 3400. Y=a*x+b -> Y=34*%.
		MAS1_Amplifier_Value = MAS_Vibration_Percentage_HMI*34;
		analogWrite(MAS2_AMPLIFIER, MAS1_Amplifier_Value);
	}
	else
	{
		digitalWrite(MAS1_DIGITAL, LOW);
		analogWrite(MAS2_AMPLIFIER, 0);
	}
}
