/*
 * Conductive_Mode_Function.cpp
 * Created: 3-7-2018 12:53:03
 *  Author: RK
 */ 

//Includes
#include "Main_Siemens_Automation_824MSPe.h"

//Local Variables.
bool Read_SS_Coil_Active;

//Timer Variables.
unsigned long Timer_12;
//Set timer to true when timer is finished.
bool TimedOut_12 = false;
//Amount of time the Timer is active.
unsigned long INTERVAL_12;
//Fastener Detection Delay Var.
bool CM_Delay;
//Fastener Detection Delay Time.
int CM_Delay_Time = 800;

//CET set value.
int CetValue_CM_Function;

//Setup Code.
void Setup_Conductive_Mode_Function_Function(void)
{
	//Timer initialization.
	TimedOut_12 = false;
	//Start Timer.
	Timer_12 = millis();
}

//Function Code.
void Conductive_Mode_Function(void)
{
	//Read analog CET value.
	CetValue_CM_Function = Analog_CET;
	
	if (ConductiveM_HMI == false)
	{
		if (Safety_Sensor_Error == true || Safety_Sensor_Switched_State == true && Toolingcontact_Switched_State == false || CM_Delay == true)
		{
			Safety_Sensor_Error = true;
		}
	}
	//Read coils. Reset SS error. MCU is master at reset. HMI will get only ACK.
	if (!Safety_Sensor_Error == true && !SSSF_Safety_Sensor_Error == true && !TPS_Error == true && !Fastener_Detection_Error == true && !Fastener_Length_Error == true && !FD_Retry_Set == true && !Shuttle_Extend_Error == true && !Shuttle_Retract_Error == true)
	{
		Error_Set_HMI = 0;
		Error_Number_HMI = 0;
	}
	//Write coils. Set HMI into error mode. Display SS error by error number 1.
	if (Safety_Sensor_Error == true || SSSF_Safety_Sensor_Error == true)
	{
		Read_Coil_48 = false;
		Write_Coil_48 = true;
		Read_Coil_73 = false;
		Write_Coil_73 = true;
		Error_Set_HMI = 1;
		Error_Number_HMI = 1;
	}
	//Conductive Mode Timer. Delay Error Reset Signal (500ms).
	if ((!TimedOut_12) && ((millis() - Timer_12) > INTERVAL_12))
	{
		//Timed out to deactivate timer.
		TimedOut_12 = true;

		//Check if CM Delay is activated.
		if (CM_Delay == true)
		{
			//De-active Timer.
			CM_Delay = false;
		}
	}
	//Set CM Delay Time.
	{
		INTERVAL_12 = CM_Delay_Time;
	}
	//Timer Start. Activate CM Delay Timer.
	if ((Safety_Sensor_Error == true || SSSF_Safety_Sensor_Error == true) && (CetValue_CM_Function < TOS) && (INTERVAL_12 > 0))
	{
		TimedOut_12 = false;
		Timer_12 = millis();
		CM_Delay = true;
	}
}